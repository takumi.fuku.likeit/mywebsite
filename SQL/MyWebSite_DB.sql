﻿--データベース
CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;

--宣言
USE mywebsite;

--userテーブル作成
CREATE TABLE user (
    user_id int PRIMARY KEY AUTO_INCREMENT COMMENT 'ユーザーID',
    mail_address varchar(255) Not Null COMMENT 'メールアドレス',
    password varchar(255) Not Null COMMENT 'パスワード'
    )

--userテーブルデータ
INSERT INTO user(user_id, mail_address, password) VALUES
    (0, 'user1@sample.com', 'password1'),
    (0, 'user2@sample.com', 'password2')



--partyテーブル
CREATE TABLE party (
    party_id int PRIMARY KEY AUTO_INCREMENT COMMENT 'パーティID',
    party_name varchar(255) UNIQUE Not Null COMMENT 'パーティ名',
    doshare boolean COMMENT '共有フラグ',
    user_id int Not Null COMMENT 'ユーザーID'
    )

--データ挿入
INSERT INTO party VALUES (0, 'ザシオーガ', 1, 1)




--natureテーブル
CREATE TABLE nature (
    nature_id int PRIMARY KEY AUTO_INCREMENT COMMENT 'せいかくID',
    nature_name varchar(255) UNIQUE Not Null COMMENT 'せいかく名'
    )
    
--natureテーブルデータ
INSERT INTO nature(nature_id, nature_name) VALUES
    (0, 'さみしがり'), (0, 'いじっぱり'), (0, 'やんちゃ'), (0, 'ゆうかん'), (0, 'ずぶとい'), (0, 'わんぱく'), (0, 'のうてんき'), (0, 'のんき'),
    (0, 'ひかえめ'), (0, 'おっとり'), (0, 'うっかりや'), (0, 'れいせい'), (0, 'おだやか'), (0, 'おとなしい'), (0, 'しんちょう'), (0, 'なまいき'),
     (0, 'おくびょう'), (0, 'せっかち'), (0, 'ようき'), (0, 'むじゃき'), (0, '無補正')



--typeテーブル
CREATE TABLE type (
    type_id int PRIMARY KEY AUTO_INCREMENT COMMENT 'タイプID',
    type_name varchar(255) UNIQUE Not Null COMMENT 'タイプ名'
    )

--typeテーブルデータ
INSERT INTO type(type_id, type_name) VALUES
(0, 'ノーマル'), (0, 'ほのお'), (0, 'みず'), (0, 'でんき'),
(0, 'くさ'), (0, 'こおり'), (0, 'かくとう'), (0, 'どく'),
(0, 'じめん'), (0, 'ひこう'), (0, 'エスパー'), (0, 'むし'),
(0, 'いわ'), (0, 'ゴースト'), (0, 'ドラゴン'), (0, 'あく'),
(0, 'はがね'), (0, 'フェアリー'), (0, '無し')



--itemテーブル
CREATE TABLE item (
    item_id int PRIMARY KEY AUTO_INCREMENT COMMENT 'アイテムID',
    item_name varchar(255) UNIQUE Not Null COMMENT 'アイテム名'
    )

--itemテーブル
INSERT INTO item(item_id, item_name) VALUES
(0, 'くちたけん'), (0, 'くろいヘドロ'), (0, 'きあいのタスキ'),
(0, 'いのちのたま'), (0, 'とつげきチョッキ'), (0, 'こだわりスカーフ'),
(0, 'こだわりハチマキ'), (0, 'こだわりメガネ'), (0, 'ラムのみ'),
(0, 'オボンのみ'), (0, 'ゴツゴツメット'), (0, 'たべのこし'), (0, 'イバンのみ'),
(0, 'パワーハーブ'), (0, 'しんかのきせき')



--pokemonテーブル
CREATE TABLE pokemon (
    pokemon_id int PRIMARY KEY AUTO_INCREMENT COMMENT 'ポケモンID',
    pokemon_name varchar(255) UNIQUE Not Null COMMENT '名前',
    ability1 varchar(255) COMMENT 'とくせい1',
    ability2 varchar(255) COMMENT 'とくせい2',
    hidden_ability varchar(255) COMMENT '隠れとくせい',
    type_id int Not Null COMMENT 'タイプID',
    type2_id int COMMENT 'タイプ2ID'
    )

--ポケモンデータ
INSERT INTO pokemon(pokemon_id, pokemon_name, ability1, ability2, hidden_ability, type_id, type2_id) VALUES
(0, 'ザシアン', 'ふとうのけん', '無し', '無し', 17, 18),
(0, 'カイオーガ', 'あめふらし', '無し', '無し', 3, 19),
(0, 'サンダー', 'プレッシャー', '無し', 'せいでんき', 4, 10),
(0, 'ヌケニン', 'ふしぎなまもり', '無し', '無し', 12, 14),
(0, 'カバルドン', 'すなおこし', '無し', 'すなのちから', 9, 19),
(0, 'トリトドン', 'ねんちゃく', 'よびみず', 'すなのちから', 3, 9),
(0, 'バドレックス(黒)', 'じんばいったい', '無し', '無し', 11, 14),
(0, 'ランドロス(霊)', 'いかく', '無し', '無し', 9, 10),
(0, 'ポリゴン2', 'トレース', 'ダウンロード', 'アナライズ', 1, 19),
(0, 'ガマゲロゲ', 'すいすい', 'どくしゅ', 'ちょすい', 3, 9)



--party_detailテーブル
CREATE TABLE party_detail (
    detail_id int PRIMARY KEY AUTO_INCREMENT COMMENT '詳細ID',
    move1 varchar(255) COMMENT 'わざ1',
    move2 varchar(255) COMMENT 'わざ2',
    move3 varchar(255) COMMENT 'わざ3',
    move4 varchar(255) COMMENT 'わざ4',
    party_id int Not Null COMMENT 'パーティID',
    nature_id int  COMMENT 'せいかくID',
    item_id int COMMENT 'アイテムID',
    pokemon_id int COMMENT 'ポケモンID',
    pokemon_ability varchar(255) COMMENT 'とくせい'
    )


--partyデータ挿入
INSERT INTO party_detail(detail_id, move1, move2, move3, move4, party_id, nature_id, item_id, pokemon_id, pokemon_ability) VALUES
(0, 'きょじゅうざん', 'じゃれつく', 'ほのおのキバ', 'でんこうせっか', 1, 2, 1, 1, 'ふとうのけん'),
(0, 'しおふき', 'かみなり', 'れいとうビーム', 'めいそう', 1, 9, 4, 2, 'あめふらし'),
(0, 'ボルトチェンジ', 'ねっぷう', 'ぼうふう', 'はねやすめ', 1, 5, 11, 3, 'せいでんき'),
(0, 'ポルターガイスト', 'かげうち', 'おにび', 'こらえる', 1, 1, 3, 4, 'ふしぎなまもり'),
(0, 'あくび', 'じしん', 'こおりのキバ', 'ステロ', 1, 6, 10, 5, 'すなおこし'),
(0, 'ねっとう', 'じしん', 'あくび', 'じこさいせい', 1, 16, 12, 6, 'よびみず')





