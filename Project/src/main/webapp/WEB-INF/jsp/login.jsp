<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>



<head>
    <meta charset="UTF-8">
    <title>login</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row alert alert-info">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="SharePageServlet">共有ホーム</a>
                </li>
            </ul>
        </nav>
    </header>
    <div class="row">
			<div class="col-6 offset-3 mb-5">
				<c:if test="${errMsg != null}">
					<div class="alert alert-danger" role="alert">${errMsg}</div>
				</c:if>
				<c:if test="${comAdd != null}">
					<div class="alert alert-primary" role="alert">${comAdd}</div>
				</c:if>
				<c:if test="${comLogout != null}">
					<div class="alert alert-success" role="alert">${comLogout}</div>
				</c:if>
			</div>
	</div>
    <form class="mt-5" action="LoginServlet" method="post">
        <div class="text-center mb-4">
            <h1 class="h3 mb-3">ログイン</h1>
        </div>
        <div class="row mb-4 col-10 mx-auto">
            <label>メールアドレス</label>
            <input class="ml-3" type="text" name="mailAddress" value="${mailAddress}">
        </div>
        <div class="row mb-4 col-10 mx-auto">
            <label>パスワード</label>
            <input class="ml-3" type="password" name="password">
        </div>
        <div class="col-4 mb-4 mx-auto">
            <button class="btn btn-primary" type="submit">ログイン</button>
        </div>
        <div class="col-4 mx-auto">
            <a href="UserAddServlet">新規登録</a>
        </div>
    </form>




</body>

</html>