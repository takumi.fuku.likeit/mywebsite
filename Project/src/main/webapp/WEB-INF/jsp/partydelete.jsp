<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>partydelete</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row alert alert-info">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="SharePageServlet">共有ホーム</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="MyPageServlet?userId=${user.userId}">マイページ</a>
                </li>
            </ul>
        </nav>
    </header>
    <form class="mt-5" action="PartyDeleteServlet" method="post">
        <div class="text-center mb-4">
            <h1 class="h3 mb-3">削除ページ</h1>
        </div>
        <div class="text-center mb-4">
        <input type="hidden" name="partyId" value="${party.partyId}">
            <h1 class="h5 mb-3">パーティ名：${party.partyName}</h1>
        </div>
        <div class="text-center mb-4">
            <h1 class="h5 mb-3">を削除しますか？</h1>
        </div>
        <div class="text-center">
            <a href="MyPageServlet" class="btn btn-primary">いいえ</a>
            <button class="btn btn-danger" type="submit">はい</button>
        </div>
    </form>
</body>


</html>