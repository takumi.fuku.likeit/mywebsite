<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>mypage</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row alert alert-info">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="SharePageServlet">共有ホーム</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
            </ul>
        </nav>
    </header>
    <form class="mt-5" action="MyPageServlet" method="post">
        <div>
            <h1 class="h3 mb-3">マイページ</h1>
        </div>
        <div class="mb-4 col-8 mx-auto">
            <label>パーティ名</label>
            <input class="ml-3" type="text" name="party-name">
            <label  class="ml-3">共有する</label>
            <input type="checkbox" name="doshare" value="true">
            <button class="btn btn-primary ml-3" type="submit">登録</button>
        </div>
    </form>
        <div class="col-10 mx-auto text-center mt-5">
            <table width="600" border="1">
                <thead>
                    <tr>
                        <th class="center" style="width: 15%">パーティ名</th>
                        <th style="width: 5%">更新</th>
                        <th style="width: 5%">削除</th>
                    </tr>
                </thead>
                <tbody>
                <c:if test="${partyList.size() == 0}">
                	<tr>
                    	<td>未登録</td>
                        <td class="center">------</td>
                        <td class="center">------</td>
                    </tr>
                </c:if>
                <c:forEach var="party" items="${partyList}">
                    <tr>
                    	<td>${party.partyName}</td>
                        <td class="center"><a href="http://localhost:8080/MyWebSite/PartyUpdateServlet?partyId=${party.partyId}" class="btn btn-primary">更新</a></td>
                        <td class="center"><a href="PartyDeleteServlet?partyId=${party.partyId}" class="btn btn-primary">削除</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    

</body>

</html>