<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>



<head>
    <meta charset="UTF-8">
    <title>partyupdate</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row alert alert-info">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="SharePageServlet">共有ホーム</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="MyPageServlet?userId=${user.userId}">マイページ</a>
                </li>
            </ul>
        </nav>
    </header>
    <form action="PartyNameAndShareChange" method="post">
        <div>
            <h1 class="h3 mb-3">更新ページ</h1>
        </div>
        <div class="mb-1 col-9 mx-auto">
        	<input type="hidden" name="partyId" value="${partyData.partyId}">
            <label>パーティ名</label>
            <input class="ml-3" type="text" name="partyName" value="${partyData.partyName}">
            <label>共有する</label>
            <c:if test="${partyData.doShare == false}">
            	<input class="ml-3" type="checkbox" name="doshare" value="true">
            </c:if>
            <c:if test="${partyData.doShare == true}">
            	<input class="ml-3" type="checkbox" name="doshare" value="true" checked>
            </c:if>
            <button type="submit" class="btn btn-primary">更新</button>
        </div>
    </form>
        <div class="col-12 mx-auto text-center mt-5">
            <table width="1200" border="1">
                <thead>
                    <tr>
                        <th class="center" style="width: 8%">名前</th>
                        <th class="center" style="width: 10%">特性</th>
                        <th class="center" style="width: 8%">性格</th>
                        <th class="center" style="width: 8%">持ち物</th>
                        <th class="center" style="width: 8%">わざ1</th>
                        <th class="center" style="width: 8%">わざ2</th>
                        <th class="center" style="width: 8%">わざ3</th>
                        <th class="center" style="width: 8%">わざ4</th>
                        <th class="center" style="width: 8%"></th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="party" items="${partyDetail}">
                    <tr>
                    <form action="DetailUpdateServlet" method="post">
                    <input type="hidden" name="detailId" value="${party.detailId}">
                    <input type="hidden" name="partyId" value="${party.partyId}">
                        <td class="center">
                        	<a href="http://localhost:8080/MyWebSite/PokemonListServlet?detailId=${party.detailId}&partyId=${partyData.partyId}">${party.pokemonName}</a>
                        </td>
                        <td class="center">${party.selectedAbility}</td>
                        <td class="center">
                            <select name="natureId">
                            <c:forEach var="nature" items="${natureList}">
	                            <c:if test="${nature.natureId == party.natureId}">
	                                <option value="${nature.natureId}" selected>${nature.natureName}</option>
	                            </c:if>
	                            <c:if test="${nature.natureId != party.natureId}">
	                                <option value="${nature.natureId}">${nature.natureName}</option>
	                            </c:if>
                            </c:forEach>
                            </select>
                        </td>
                        <td class="center">
                            <select name="itemId">
                            <c:forEach var="item" items="${itemList}">
	                            <c:if test="${item.itemId == party.itemId}">
	                                <option value="${item.itemId}" selected>${item.itemName}</option>
	                            </c:if>
	                            <c:if test="${item.itemId != party.itemId}">
	                                <option value="${item.itemId}">${item.itemName}</option>
	                            </c:if>
                            </c:forEach>
                            </select>
                        </td>
                        <td class="center"><input class="ml-3" type="text" size="10" name="move1" value="${party.move1}"></td>
                        <td class="center"><input class="ml-3" type="text" size="10" name="move2" value="${party.move2}"></td>
                        <td class="center"><input class="ml-3" type="text" size="10" name="move3" value="${party.move3}"></td>
                        <td class="center"><input class="ml-3" type="text" size="10" name="move4" value="${party.move4}"></td>
                        <td class="center"><button type="submit" class="btn btn-primary">更新</button></td>
                    </form>
                    </tr>
                </c:forEach>
                <c:if test="${partyDetail.size() < 6}">
                	<tr>
                        <td class="center"><a href="http://localhost:8080/MyWebSite/PokemonListServlet?detailId=0&partyId=${partyData.partyId}">未登録</a></td>
                        <td class="center">未登録</td>
                        <td class="center">未登録</td>
                        <td class="center">未登録</td>
                        <td class="center">未登録</td>
                        <td class="center">未登録</td>
                        <td class="center">未登録</td>
                        <td class="center">未登録</td>
                    </tr>
                </c:if>
                </tbody>
            </table>
        </div>
    	<div class="text-right mt-5">
            <a href="MyPageServlet">戻る</a>
        </div>

</body>

</html>