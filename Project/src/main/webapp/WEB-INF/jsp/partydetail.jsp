<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>



<head>
    <meta charset="UTF-8">
    <title>partydetail</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row alert alert-info">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="SharePageServlet">共有ホーム</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="MyPageServlet?userId=${user.userId}">マイページ</a>
                </li>
            </ul>
        </nav>
    </header>
        <div>
            <h1 class="h3 mb-3">詳細ページ</h1>
        </div>
        <div class="mb-1 col-9 mx-auto">
        	<h4>パーティ名：${party.partyName}</h4>
        </div>
        <div class="col-12 mx-auto text-center mt-5">
            <table width="1200" border="1">
                <thead>
                    <tr>
                        <th class="center" style="width: 8%">名前</th>
                        <th class="center" style="width: 10%">特性</th>
                        <th class="center" style="width: 8%">性格</th>
                        <th class="center" style="width: 8%">持ち物</th>
                        <th class="center" style="width: 8%">わざ1</th>
                        <th class="center" style="width: 8%">わざ2</th>
                        <th class="center" style="width: 8%">わざ3</th>
                        <th class="center" style="width: 8%">わざ4</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="party" items="${partyDetail}">
                    <tr>
                        <td class="center">${party.pokemonName}</td>
                        <td class="center">${party.selectedAbility}</td>
                        <td class="center">${party.natureName}</td>
                        <td class="center">${party.itemName}</td>
                        <td class="center">${party.move1}</td>
                        <td class="center">${party.move2}</td>
                        <td class="center">${party.move3}</td>
                        <td class="center">${party.move4}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="text-right mt-5">
        	<a href="SharePageServlet" class="btn btn-primary">戻る</a>
        </div>

</body>

</html>