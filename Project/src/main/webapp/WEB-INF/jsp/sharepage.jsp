<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>sharepage</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row alert alert-info">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
            </ul>
            <ul class="navbar-nav flex-row">
                <c:if test="${user != null}">
                	<li class="nav-item">
                    	<a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                	</li>
                </c:if>
                <li class="nav-item">
                    <a class="btn btn-primary" href="MyPageServlet?userId=${user.userId}">マイページ</a>
                </li>
            </ul>
        </nav>
    </header>
    <div>
        <h1 class="h3 mb-3">ポケモン一覧</h1>
    </div>
    <form action="SharePageServlet" method="post">
        <div class="mb-4 col-9 mx-auto">
            <label>禁止伝説</label>
            <select name="legend">
            	<option value="0" selected>------</option>
                <c:forEach var="legend" items="${legendList}">
                	<c:if test="${legend.pokemonId == legendId}">
                		<option value="${legend.pokemonId}" selected>${legend.pokemonName}</option>
                	</c:if>
                	<c:if test="${legend.pokemonId != legendId}">
                		<option value="${legend.pokemonId}">${legend.pokemonName}</option>
                	</c:if>
                </c:forEach>
            </select>
            <button class="btn btn-primary" type="submit">検索</button>
        </div>
    </form>
        <div class="col-12 mx-auto text-center mt-5">
            <table width="500" border="1">
                <thead>
                    <tr>
                        <th class="center" style="width: 15%">パーティ名</th>
                        <th style="width: 10%">詳細</th>
                    </tr>
                </thead>
                <tbody>
                <c:if test="${partyList.size() == 0}">
                	<tr>
                        <td class="center">共有されているパーティはありません。</td>
                        <td class="center">------</td>
                    </tr>
                </c:if>
                <c:forEach var="party" items="${partyList}">
                    <tr>
                        <td class="center">${party.partyName}</td>
                        <td class="center"><a href="PartyDetailServlet?partyId=${party.partyId}" class="btn btn-primary">詳細</a></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
    	</div>
</body>


</html>