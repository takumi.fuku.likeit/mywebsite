<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>pokemonlist</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <header>
        <nav class="navbar navbar-dark navbar-expand flex-md-row alert alert-info">
            <ul class="navbar-nav navbar-dark flex-row mr-auto header-two">
                <li class="nav-item active">
                    <a class="nav-link" href="SharePageServlet">共有ホーム</a>
                </li>
            </ul>
            <ul class="navbar-nav flex-row">
                <li class="nav-item">
                    <a class="btn btn-primary" href="LogoutServlet">ログアウト</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-primary" href="MyPageServlet?userId=${user.userId}">マイページ</a>
                </li>
            </ul>
        </nav>
    </header>
    <div>
        <h1 class="h3 mb-3">ポケモン一覧</h1>
    </div>
    <form action="PokemonListServlet" method="post">
    	<input type="hidden" name="detailId" value="${detailId}">
        <input type="hidden" name="partyId" value="${partyId}">
        <div class="mb-4 col-9 mx-auto">
            <label>タイプ</label>
            <select name="type1">
            	<option value="" selected>------</option>
            <c:forEach var="type" items="${typeList}">
            <c:if test="${type.typeId != 19}">
	            <c:if test="${type.typeId == typeId}">
		        	<option value="${type.typeId}" selected>${type.typeName}</option>
		        </c:if>
		        <c:if test="${type.typeId != typeId}">
		        	<option value="${type.typeId}">${type.typeName}</option>
		        </c:if>
            </c:if>
            </c:forEach>
            </select>
            <label>タイプ</label>
            <select name="type2">
            	<option value="" selected></option>
            <c:forEach var="type" items="${typeList}">
            	<c:if test="${type.typeId == type2Id}">
		        	<option value="${type.typeId}" selected>${type.typeName}</option>
		        </c:if>
		        <c:if test="${type.typeId != type2Id}">
		        	<option value="${type.typeId}">${type.typeName}</option>
		        </c:if>
            </c:forEach>
            </select>
            <button class="btn btn-primary" type="submit">検索</button>
        </div>
    </form>
        <div class="col-10 mx-auto text-center mt-5">
        
            <table width="1000" border="1">
                <thead>
                    <tr>
                        <th class="center" style="width: 8%">名前</th>
                        <th class="center" style="width: 7%">タイプ1</th>
                        <th class="center" style="width: 7%">タイプ2</th>
                        <th class="center" style="width: 10%">特性</th>
                        <th class="center" style="width: 8%">採用</th>
                    </tr>
                </thead>
                <tbody>
                <c:forEach var="pokemon" items="${pokemonList}">
                    <tr>
                    <form action="PokemonToDetailServlet" method="post">
                    	<input type="hidden" name="detailId" value="${detailId}">
        				<input type="hidden" name="partyId" value="${partyId}">
                    	<input type="hidden" name="pokemonId" value="${pokemon.pokemonId}">
                        <td class="center">${pokemon.pokemonName}</td>
                        <td class="center">${pokemon.type1Name}</td>
                        <td class="center">${pokemon.type2Name}</td>
                        <td class="center">
                            <select name="ability">
                                <option value="${pokemon.ability1}">${pokemon.ability1}</option>
                                <c:if test="${!pokemon.ability2.equals('無し')}">
                                	<option value="${pokemon.ability2}">${pokemon.ability2}</option>
                                </c:if>
                                <c:if test="${!pokemon.ability3.equals('無し')}">
                                	<option value="${pokemon.ability3}">${pokemon.ability3}</option>
                                </c:if>
                            </select>
                        </td>
                        <td class="center"><button type="submit" class="btn btn-primary">確定</button></td>
                    </form>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    
</body>


</html>