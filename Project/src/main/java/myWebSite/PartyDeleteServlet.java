package myWebSite;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.PartyBeans;
import beans.UserBeans;
import dao.DeleteDao;
import dao.UpdateDao;

/**
 * Servlet implementation class PartyDeleteServlet
 */
@WebServlet("/PartyDeleteServlet")
public class PartyDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PartyDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("user");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      String getPartyId = request.getParameter("partyId");
      int partyId = Integer.valueOf(getPartyId);

      PartyBeans party = UpdateDao.findPartyData(partyId);

      request.setAttribute("party", party);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/partydelete.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      String getPartyId = request.getParameter("partyId");
      int partyId = Integer.valueOf(getPartyId);

      DeleteDao.partyDetailDelete(partyId);
      DeleteDao.partyDelete(partyId);

      response.sendRedirect("MyPageServlet");
	}

}
