package myWebSite;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.PokemonBeans;
import beans.TypeBeans;
import beans.UserBeans;
import dao.PokemonListDao;

/**
 * Servlet implementation class PokemonListServlet
 */
@WebServlet("/PokemonListServlet")
public class PokemonListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PokemonListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("user");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      String getDetailId = request.getParameter("detailId");
      int detailId = Integer.valueOf(getDetailId);
      String getPartyId = request.getParameter("partyId");
      int partyId = Integer.valueOf(getPartyId);
      ArrayList<TypeBeans> typeList = PokemonListDao.findTypeAll();
      ArrayList<PokemonBeans> pokemonList = PokemonListDao.findPokemonAll();
      request.setAttribute("detailId", detailId);
      request.setAttribute("partyId", partyId);
      request.setAttribute("typeList", typeList);
      request.setAttribute("pokemonList", pokemonList);


      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pokemonlist.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String getDetailId = request.getParameter("detailId");
	    int detailId = Integer.valueOf(getDetailId);
	    String getPartyId = request.getParameter("partyId");
	    int partyId = Integer.valueOf(getPartyId);
        String getTypeId = request.getParameter("type1");
        int typeId = 0;
        String getType2Id = request.getParameter("type2");
        int type2Id = 0;

        if (getTypeId.equals("") && getType2Id.equals("")) {
          ArrayList<PokemonBeans> pokemonList = PokemonListDao.findPokemonAll();
          request.setAttribute("pokemonList", pokemonList);

        } else if (!getTypeId.equals("") && getType2Id.equals("")) {
          typeId = Integer.valueOf(getTypeId);
          ArrayList<PokemonBeans> pokemonList = PokemonListDao.serchPokemonByType1(typeId);
          request.setAttribute("pokemonList", pokemonList);

        } else if (getTypeId.equals("") && !getType2Id.equals("")) {
          type2Id = Integer.valueOf(getType2Id);
          ArrayList<PokemonBeans> pokemonList = PokemonListDao.serchPokemonByType1(type2Id);
          request.setAttribute("pokemonList", pokemonList);

        } else {
          typeId = Integer.valueOf(getTypeId);
          type2Id = Integer.valueOf(getType2Id);
          ArrayList<PokemonBeans> pokemonList = PokemonListDao.serchPokemonByType2(typeId, type2Id);
          request.setAttribute("pokemonList", pokemonList);

        }

        ArrayList<TypeBeans> typeList = PokemonListDao.findTypeAll();
        request.setAttribute("typeId", typeId);
        request.setAttribute("type2Id", type2Id);
        request.setAttribute("typeList", typeList);
        request.setAttribute("detailId", detailId);
        request.setAttribute("partyId", partyId);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/pokemonlist.jsp");
        dispatcher.forward(request, response);

	}

}
