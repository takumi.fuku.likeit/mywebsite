package myWebSite;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.ItemBeans;
import beans.NatureBeans;
import beans.PartyBeans;
import beans.PartyDetailBeans;
import beans.UserBeans;
import dao.UpdateDao;

/**
 * Servlet implementation class PartyUpdateServlet
 */
@WebServlet("/PartyUpdateServlet")
public class PartyUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PartyUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("user");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      String getPartyId = request.getParameter("partyId");
      int partyId = Integer.valueOf(getPartyId);
      PartyBeans partyData = UpdateDao.findPartyData(partyId);
      ArrayList<PartyDetailBeans> partyDetail = UpdateDao.findPartyDetail(partyId);
      ArrayList<ItemBeans> itemList = UpdateDao.findItemAll();
      ArrayList<NatureBeans> natureList = UpdateDao.findNatureAll();
      request.setAttribute("partyData", partyData);
      request.setAttribute("partyDetail", partyDetail);
      request.setAttribute("itemList", itemList);
      request.setAttribute("natureList", natureList);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/partyupdate.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
