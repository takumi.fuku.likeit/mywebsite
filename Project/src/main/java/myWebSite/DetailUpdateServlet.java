package myWebSite;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.UpdateDao;

/**
 * Servlet implementation class DetailUpdateServlet
 */
@WebServlet("/DetailUpdateServlet")
public class DetailUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DetailUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("user");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }


      String getDetailId = request.getParameter("detailId");
      int detailid = Integer.valueOf(getDetailId);
      String getPartyId = request.getParameter("partyId");
      int partyId = Integer.valueOf(getPartyId);
      String getNatureId = request.getParameter("natureId");
      int natureId = Integer.valueOf(getNatureId);
      String getItemId = request.getParameter("itemId");
      int itemId = Integer.valueOf(getItemId);
      String move1 = request.getParameter("move1");
      String move2 = request.getParameter("move2");
      String move3 = request.getParameter("move3");
      String move4 = request.getParameter("move4");

      UpdateDao.detailUpdate(move1, move2, move3, move4, natureId, itemId, detailid);

      response.sendRedirect(request.getContextPath() + "/PartyUpdateServlet?partyId=" + partyId);
	}

}
