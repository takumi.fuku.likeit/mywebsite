package myWebSite;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.PartyBeans;
import beans.PartyDetailBeans;
import dao.UpdateDao;

/**
 * Servlet implementation class PartyDetailServlet
 */
@WebServlet("/PartyDetailServlet")
public class PartyDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PartyDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      String getPartyId = request.getParameter("partyId");
      int partyId = Integer.valueOf(getPartyId);
      ArrayList<PartyDetailBeans> partyDetail = UpdateDao.findPartyDetail(partyId);
      PartyBeans party = UpdateDao.findPartyData(partyId);
      request.setAttribute("partyDetail", partyDetail);
      request.setAttribute("party", party);

      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/partydetail.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
