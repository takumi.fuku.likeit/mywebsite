package myWebSite;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.PartyBeans;
import beans.PokemonBeans;
import dao.SharePageDao;

/**
 * Servlet implementation class SharePageServlet
 */
@WebServlet("/SharePageServlet")
public class SharePageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SharePageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      ArrayList<PartyBeans> partyList = SharePageDao.findShareParty();
      ArrayList<PokemonBeans> legendList = SharePageDao.findLegend();
      request.setAttribute("partyList", partyList);
      request.setAttribute("legendList", legendList);
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sharepage.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");

      String getLegendId = request.getParameter("legend");
      int legendId = Integer.valueOf(getLegendId);
      if (legendId == 0) {
        ArrayList<PartyBeans> partyList = SharePageDao.findShareParty();
        ArrayList<PokemonBeans> legendList = SharePageDao.findLegend();
        request.setAttribute("partyList", partyList);
        request.setAttribute("legendList", legendList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sharepage.jsp");
        dispatcher.forward(request, response);
        return;
      }

      ArrayList<PartyBeans> partyList = SharePageDao.searchPartyByLegend(legendId);
      ArrayList<PokemonBeans> legendList = SharePageDao.findLegend();
      request.setAttribute("partyList", partyList);
      request.setAttribute("legendList", legendList);
      request.setAttribute("legendId", legendId);
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sharepage.jsp");
      dispatcher.forward(request, response);
	}

}
