package myWebSite;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.UserBeans;
import dao.UserAddDao;
import util.PasswordEncoder;

/**
 * Servlet implementation class UserAddServlet
 */
@WebServlet("/UserAddServlet")
public class UserAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAddServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
      dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      String mailAddress = request.getParameter("mailAddress");
      String password = request.getParameter("password");
      String passwordConfirm = request.getParameter("passwordConfirm");

      if (!password.equals(passwordConfirm) || mailAddress.equals("") || password.equals("")
          || passwordConfirm.equals("")) {
        request.setAttribute("errMsg", "入力された内容は正しくありません。");
        request.setAttribute("mailAddress", mailAddress);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
        dispatcher.forward(request, response);
        return;
      }


      ArrayList<UserBeans> uList = UserAddDao.findAllUser();
      for (UserBeans u : uList) {
        if (mailAddress.equals(u.getMailAddress())) {
          request.setAttribute("errMsg", "このメールアドレスは使用されています。");
          RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/useradd.jsp");
          dispatcher.forward(request, response);
          return;
        }
      }

      password = PasswordEncoder.encodePassword(password);

      UserAddDao.userAdd(mailAddress, password);
      request.setAttribute("mailAddress", mailAddress);
      request.setAttribute("comAdd", "登録が完了しました。");
      RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
      dispatcher.forward(request, response);
	}

}
