package myWebSite;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.UpdateDao;

/**
 * Servlet implementation class PartyNameAndShareChange
 */
@WebServlet("/PartyNameAndShareChange")
public class PartyNameAndShareChange extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PartyNameAndShareChange() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("user");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      String getPartyId = request.getParameter("partyId");
      int partyId = Integer.valueOf(getPartyId);
      String partyName = request.getParameter("partyName");
      String doshare = request.getParameter("doshare");
      boolean doShare;
      if (doshare != null) {
        doShare = Boolean.valueOf(doshare);
      } else {
        doShare = false;
      }

      UpdateDao.changeNameAndShareFrag(partyName, doShare, partyId);

      response.sendRedirect(request.getContextPath() + "/PartyUpdateServlet?partyId=" + partyId);
	}

}
