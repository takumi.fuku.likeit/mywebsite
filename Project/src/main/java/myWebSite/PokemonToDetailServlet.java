package myWebSite;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.UserBeans;
import dao.PokemonListDao;

/**
 * Servlet implementation class PokemonToDetailServlet
 */
@WebServlet("/PokemonToDetailServlet")
public class PokemonToDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PokemonToDetailServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      // TODO Auto-generated method stub
      response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      request.setCharacterEncoding("UTF-8");
      HttpSession session = request.getSession();
      UserBeans u = (UserBeans) session.getAttribute("user");
      if (u == null) {
        response.sendRedirect("LoginServlet");
        return;
      }

      String getDetailId = request.getParameter("detailId");
      int detailId = Integer.valueOf(getDetailId);
      String getPartyId = request.getParameter("partyId");
      int partyId = Integer.valueOf(getPartyId);

      String pokemonAbility = request.getParameter("ability");
      String getPokemonId = request.getParameter("pokemonId");
      int pokemonId = Integer.valueOf(getPokemonId);


      if (detailId == 0) {
        PokemonListDao.insertPokemonToDetail(detailId, partyId, pokemonId, pokemonAbility);
      } else {
        PokemonListDao.UpdatePokemonToDetail(detailId, pokemonId, pokemonAbility);
      }

      response.sendRedirect(request.getContextPath() + "/PartyUpdateServlet?partyId=" + partyId);
	}

}
