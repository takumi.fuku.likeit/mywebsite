package base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
  private static final String url =
      "jdbc:mysql://localhost/mywebsite?useUnicode=true&characterEncoding=utf8";
  private static final String user = "root";
  private static final String pass = "stneuaz5648";

  // 接続
  public static Connection getConnection() {
    Connection con = null;

    try {
      Class.forName("com.mysql.jdbc.Driver");
      con = DriverManager.getConnection(url, user, pass);

    } catch (SQLException | ClassNotFoundException e) {
      e.printStackTrace();
    }
    return con;
  }

}