package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import base.DBManager;

public class DeleteDao {
  public static void partyDetailDelete(int partyId) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "DELETE FROM party_detail WHERE party_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, partyId);
      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  public static void partyDelete(int partyId) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "DELETE FROM party WHERE party_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, partyId);
      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

}
