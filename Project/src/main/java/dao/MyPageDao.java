package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import base.DBManager;
import beans.PartyBeans;

public class MyPageDao {
  public static ArrayList<PartyBeans> findPartyData(int userId) {
    Connection conn = null;
    ArrayList<PartyBeans> partyList = new ArrayList<PartyBeans>();
    
    try {
      conn = DBManager.getConnection();
      
      String sql = "SELECT * FROM party WHERE user_id = ?";
      
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, userId);
      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        PartyBeans party = new PartyBeans();
        party.setUserId(userId);
        party.setPartyId(rs.getInt("party_id"));
        party.setPartyName(rs.getString("party_name"));
        party.setDoShare(rs.getBoolean("doshare"));
        partyList.add(party);
      }
      
      
      
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
      } finally {
        // データベース切断
        if (conn != null) {
          try {
            conn.close();
          } catch (SQLException e) {
            e.printStackTrace();
            return null;
          }
        }
    }

    return partyList;
  }



  public static void partyAdd(String partyName, boolean doshare, int userId) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "INSERT INTO party VALUES (0, ?, ?, ?)";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, partyName);
      pStmt.setBoolean(2, doshare);
      pStmt.setInt(3, userId);

      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

}
