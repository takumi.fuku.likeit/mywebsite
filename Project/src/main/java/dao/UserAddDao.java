package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import base.DBManager;
import beans.UserBeans;

public class UserAddDao {
  public static void userAdd(String mailAddress, String password) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "INSERT INTO user VALUES (0, ?, ?)";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, mailAddress);
      pStmt.setString(2, password);

      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  public static ArrayList<UserBeans> findAllUser() {
    Connection conn = null;
    ArrayList<UserBeans> userList = new ArrayList<UserBeans>();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user";

      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        UserBeans user = new UserBeans();
        user.setUserId(rs.getInt("user_id"));
        user.setMailAddress(rs.getString("mail_address"));
        user.setPassword("password");
        userList.add(user);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return userList;

  }


}
