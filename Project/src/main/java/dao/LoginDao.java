package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import base.DBManager;
import beans.UserBeans;

public class LoginDao {
  public static UserBeans findUserData(String mailAddress, String password) {
    Connection conn = null;
    UserBeans user = new UserBeans();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM user WHERE mail_address = ? AND password = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, mailAddress);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      if (!rs.next()) {
        return null;
      }

      user.setMailAddress(rs.getString("mail_address"));
      user.setPassword(rs.getString("password"));
      user.setUserId(rs.getInt("user_id"));


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return user;

  }

}
