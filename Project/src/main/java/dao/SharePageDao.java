package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import base.DBManager;
import beans.PartyBeans;
import beans.PokemonBeans;

public class SharePageDao {
  public static ArrayList<PartyBeans> findShareParty(){
    Connection conn = null;
    ArrayList<PartyBeans> partyList = new ArrayList<PartyBeans>();
    
    try {
      conn = DBManager.getConnection();
      
      String sql = "SELECT * FROM party WHERE doshare = true";
      
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        PartyBeans party = new PartyBeans();
        party.setUserId(rs.getInt("user_id"));
        party.setPartyId(rs.getInt("party_id"));
        party.setPartyName(rs.getString("party_name"));
        party.setDoShare(rs.getBoolean("doshare"));
        partyList.add(party);
      }
      
      
      
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
      } finally {
        // データベース切断
        if (conn != null) {
          try {
            conn.close();
          } catch (SQLException e) {
            e.printStackTrace();
            return null;
          }
        }
    }

    return partyList;
  }



  public static ArrayList<PartyBeans> searchPartyByLegend(int legendId) {
    Connection conn = null;
    ArrayList<PartyBeans> partyList = new ArrayList<PartyBeans>();

    try {
      conn = DBManager.getConnection();

      String sql = "select * from party where party_id"
          + " IN (select party_id from party_detail where pokemon_id = ? and doshare = true)";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, legendId);
      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        PartyBeans party = new PartyBeans();
        party.setUserId(rs.getInt("user_id"));
        party.setPartyId(rs.getInt("party_id"));
        party.setPartyName(rs.getString("party_name"));
        party.setDoShare(rs.getBoolean("doshare"));
        partyList.add(party);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return partyList;
  }



  public static ArrayList<PokemonBeans> findLegend() {
    Connection conn = null;
    ArrayList<PokemonBeans> pokemonList = new ArrayList<PokemonBeans>();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM pokemon WHERE pokemon_id = 1 or pokemon_id = 2 or pokemon_id = 7";

      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);


      while (rs.next()) {
        int pokemonId = rs.getInt("pokemon_id");
        String pokemonName = rs.getString("pokemon_name");
        PokemonBeans pokemon = new PokemonBeans(pokemonId, pokemonName);
        pokemonList.add(pokemon);
      }

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return pokemonList;
  }
}
