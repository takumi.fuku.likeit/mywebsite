package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import base.DBManager;
import beans.PokemonBeans;
import beans.TypeBeans;

public class PokemonListDao {
  public static ArrayList<PokemonBeans> findPokemonAll() {

    Connection conn = null;
    ArrayList<PokemonBeans> pokemonList = new ArrayList<PokemonBeans>();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM pokemon p INNER JOIN type t ON p.type_id = t.type_id"
          + " ORDER BY pokemon_id ASC";
      String sql2 =
          "SELECT p.type2_id, type_name FROM pokemon p INNER JOIN type t ON p.type2_id = t.type_id"
              + " ORDER BY pokemon_id ASC";

      Statement stmt = conn.createStatement();
      Statement stmt2 = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);
      ResultSet rs2 = stmt2.executeQuery(sql2);

      while (rs.next() && rs2.next()) {
        PokemonBeans pokemon = new PokemonBeans();
        pokemon.setPokemonId(rs.getInt("pokemon_id"));
        pokemon.setPokemonName(rs.getString("pokemon_name"));
        pokemon.setAbility1(rs.getString("ability1"));
        pokemon.setAbility2(rs.getString("ability2"));
        pokemon.setAbility3(rs.getString("hidden_ability"));
        pokemon.setType1Id(rs.getInt("type_id"));
        pokemon.setType1Name(rs.getString("type_name"));
        pokemon.setType2Id(rs2.getInt("type2_id"));
        pokemon.setType2Name(rs2.getString("type_name"));
        pokemonList.add(pokemon);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return pokemonList;

  }



  public static void insertPokemonToDetail(int detailId, int partyId, int pokemonId,
      String pokemonAbility) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql =
          "INSERT INTO party_detail(detail_id, move1, move2, move3, move4, party_id, nature_id, item_id, pokemon_id, pokemon_ability) VALUES"
              + " (?, '未登録', '未登録', '未登録', '未登録', ?, 1, 1,  ?, ?)";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, detailId);
      pStmt.setInt(2, partyId);
      pStmt.setInt(3, pokemonId);
      pStmt.setString(4, pokemonAbility);

      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  public static void UpdatePokemonToDetail(int detailId, int pokemonId, String pokemonAbility) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql =
          "UPDATE party_detail SET pokemon_id = ?, pokemon_ability = ?,"
              + " move1 = '未登録', move2 = '未登録', move3 = '未登録', move4 = '未登録',"
              + " nature_id = 1, item_id = 1 WHERE detail_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, pokemonId);
      pStmt.setString(2, pokemonAbility);
      pStmt.setInt(3, detailId);

      pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }

  }



  public static ArrayList<TypeBeans> findTypeAll() {
    Connection conn = null;
    ArrayList<TypeBeans> typeList = new ArrayList<TypeBeans>();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM type ORDER BY type_id ASC";

      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        TypeBeans type = new TypeBeans();
        type.setTypeId(rs.getInt("type_id"));
        type.setTypeName(rs.getString("type_name"));
        typeList.add(type);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return typeList;

  }



  public static ArrayList<PokemonBeans> serchPokemonByType1(int typeId) {
    Connection conn = null;
    ArrayList<PokemonBeans> pokemonList = new ArrayList<PokemonBeans>();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM pokemon p INNER JOIN type t ON p.type_id = t.type_id"
          + " WHERE p.type_id = ? OR p.type2_id = ? ORDER BY pokemon_id ASC";
      String sql2 =
          "SELECT p.type2_id, type_name FROM pokemon p INNER JOIN type t ON p.type2_id = t.type_id"
              + " WHERE p.type_id = ? OR p.type2_id = ? ORDER BY pokemon_id ASC";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, typeId);
      pStmt.setInt(2, typeId);
      ResultSet rs = pStmt.executeQuery();
      PreparedStatement pStmt2 = conn.prepareStatement(sql2);
      pStmt2.setInt(1, typeId);
      pStmt2.setInt(2, typeId);
      ResultSet rs2 = pStmt2.executeQuery();

      while (rs.next() && rs2.next()) {
        PokemonBeans pokemon = new PokemonBeans();
        pokemon.setPokemonId(rs.getInt("pokemon_id"));
        pokemon.setPokemonName(rs.getString("pokemon_name"));
        pokemon.setAbility1(rs.getString("ability1"));
        pokemon.setAbility2(rs.getString("ability2"));
        pokemon.setAbility3(rs.getString("hidden_ability"));
        pokemon.setType1Id(rs.getInt("type_id"));
        pokemon.setType1Name(rs.getString("type_name"));
        pokemon.setType2Id(rs2.getInt("type2_id"));
        pokemon.setType2Name(rs2.getString("type_name"));
        pokemonList.add(pokemon);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return pokemonList;

  }
  
  
  
  public static ArrayList<PokemonBeans> serchPokemonByType2(int typeId, int type2Id) {
    Connection conn = null;
    ArrayList<PokemonBeans> pokemonList = new ArrayList<PokemonBeans>();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM pokemon p INNER JOIN type t ON p.type_id = t.type_id"
          + " WHERE (p.type_id = ? AND p.type2_id = ?) OR (p.type_id = ? AND p.type2_id = ?)"
          + " ORDER BY pokemon_id ASC";
      String sql2 =
          "SELECT p.type2_id, type_name FROM pokemon p INNER JOIN type t ON p.type2_id = t.type_id"
              + " WHERE (p.type_id = ? AND p.type2_id = ?) OR (p.type_id = ? AND p.type2_id = ?)"
              + " ORDER BY pokemon_id ASC";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, typeId);
      pStmt.setInt(2, type2Id);
      pStmt.setInt(3, type2Id);
      pStmt.setInt(4, typeId);
      ResultSet rs = pStmt.executeQuery();
      PreparedStatement pStmt2 = conn.prepareStatement(sql2);
      pStmt2.setInt(1, typeId);
      pStmt2.setInt(2, type2Id);
      pStmt2.setInt(3, type2Id);
      pStmt2.setInt(4, typeId);
      ResultSet rs2 = pStmt2.executeQuery();

      while (rs.next() && rs2.next()) {
        PokemonBeans pokemon = new PokemonBeans();
        pokemon.setPokemonId(rs.getInt("pokemon_id"));
        pokemon.setPokemonName(rs.getString("pokemon_name"));
        pokemon.setAbility1(rs.getString("ability1"));
        pokemon.setAbility2(rs.getString("ability2"));
        pokemon.setAbility3(rs.getString("hidden_ability"));
        pokemon.setType1Id(rs.getInt("type_id"));
        pokemon.setType1Name(rs.getString("type_name"));
        pokemon.setType2Id(rs2.getInt("type2_id"));
        pokemon.setType2Name(rs2.getString("type_name"));
        pokemonList.add(pokemon);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return pokemonList;

  }
}
