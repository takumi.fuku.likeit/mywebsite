package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import base.DBManager;
import beans.ItemBeans;
import beans.NatureBeans;
import beans.PartyBeans;
import beans.PartyDetailBeans;

public class UpdateDao {

  public static ArrayList<PartyDetailBeans> findPartyDetail(int partyId) {
    Connection conn = null;
    ArrayList<PartyDetailBeans> partyDetail = new ArrayList<PartyDetailBeans>();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql =
          "SELECT d.detail_id, d.party_id, d.pokemon_id, p.pokemon_name, d.pokemon_ability,"
              + " d.nature_id, n.nature_name, d.item_id, i.item_name, move1, move2, move3, move4"
              + " FROM party_detail d INNER JOIN pokemon p ON d.pokemon_id = p.pokemon_id"
              + " INNER JOIN item i ON d.item_id = i.item_id"
              + " INNER JOIN nature n ON d.nature_id = n.nature_id" + " WHERE party_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);

      pStmt.setInt(1, partyId);
      ResultSet rs = pStmt.executeQuery();

      while (rs.next()) {
        PartyDetailBeans detail = new PartyDetailBeans();
        detail.setDetailId(rs.getInt("detail_id"));
        detail.setPartyId(partyId);
        detail.setPokemonId(rs.getInt("pokemon_id"));
        detail.setPokemonName(rs.getString("pokemon_name"));
        detail.setSelectedAbility(rs.getString("pokemon_ability"));
        detail.setNatureId(rs.getInt("nature_id"));
        detail.setNatureName(rs.getString("nature_name"));
        detail.setItemId(rs.getInt("item_id"));
        detail.setItemName(rs.getString("item_name"));
        detail.setMove1(rs.getString("move1"));
        detail.setMove2(rs.getString("move2"));
        detail.setMove3(rs.getString("move3"));
        detail.setMove4(rs.getString("move4"));
        partyDetail.add(detail);
      }


    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return partyDetail;

  }


  public static ArrayList<ItemBeans> findItemAll() {

    Connection conn = null;
    ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM item ORDER BY item_id ASC";

      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        ItemBeans item = new ItemBeans();
        item.setItemId(rs.getInt("item_id"));
        item.setItemName(rs.getString("item_name"));
        itemList.add(item);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return itemList;

  }



  public static ArrayList<NatureBeans> findNatureAll() {

    Connection conn = null;
    ArrayList<NatureBeans> natureList = new ArrayList<NatureBeans>();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM nature ORDER BY nature_id ASC";

      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      while (rs.next()) {
        NatureBeans nature = new NatureBeans();
        nature.setNatureId(rs.getInt("nature_id"));
        nature.setNatureName(rs.getString("nature_name"));
        natureList.add(nature);
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return natureList;

  }


  public static PartyBeans findPartyData(int partyId) {
    Connection conn = null;
    PartyBeans partyData = new PartyBeans();

    try {
      // 接続
      conn = DBManager.getConnection();

      String sql = "SELECT * FROM party WHERE party_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setInt(1, partyId);
      ResultSet rs = pStmt.executeQuery();

      if (rs.next()) {
        partyData.setPartyId(partyId);
        partyData.setPartyName(rs.getString("party_name"));
        partyData.setDoShare(rs.getBoolean("doshare"));
        partyData.setUserId(rs.getInt("user_id"));
      }



    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }

    return partyData;

  }



  public static void changeNameAndShareFrag(String partyName, boolean doShare, int partyId) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "UPDATE party SET party_name = ?, doshare = ? WHERE party_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, partyName);
      pStmt.setBoolean(2, doShare);
      pStmt.setInt(3, partyId);

      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }



  public static void detailUpdate(String move1, String move2, String move3, String move4,
      int natureId, int itemId, int detailId) {
    Connection conn = null;

    try {
      conn = DBManager.getConnection();

      String sql = "UPDATE party_detail SET move1 = ?, move2 = ?, move3 = ?, move4 = ?,"
          + " nature_id = ?, item_id = ? WHERE detail_id = ?";

      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, move1);
      pStmt.setString(2, move2);
      pStmt.setString(3, move3);
      pStmt.setString(4, move4);
      pStmt.setInt(5, natureId);
      pStmt.setInt(6, itemId);
      pStmt.setInt(7, detailId);

      pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
  }

}
