package beans;

import java.io.Serializable;

public class PokemonBeans implements Serializable {
  private int pokemonId;
  private String pokemonName;
  private int type1Id;
  private String type1Name;
  private int type2Id;
  private String type2Name;
  private String ability1;
  private String ability2;
  private String ability3;



  public PokemonBeans() {

  }


  public PokemonBeans(int pokemonId, String pokemonName) {
    this.pokemonId = pokemonId;
    this.pokemonName = pokemonName;
  }

  public int getPokemonId() {
    return pokemonId;
  }

  public void setPokemonId(int pokemonId) {
    this.pokemonId = pokemonId;
  }

  public String getPokemonName() {
    return pokemonName;
  }

  public void setPokemonName(String pokemonName) {
    this.pokemonName = pokemonName;
  }

  public String getAbility1() {
    return ability1;
  }

  public void setAbility1(String ability1) {
    this.ability1 = ability1;
  }

  public String getAbility2() {
    return ability2;
  }

  public void setAbility2(String ability2) {
    this.ability2 = ability2;
  }

  public String getAbility3() {
    return ability3;
  }

  public void setAbility3(String ability3) {
    this.ability3 = ability3;
  }

  public int getType1Id() {
    return type1Id;
  }

  public void setType1Id(int type1Id) {
    this.type1Id = type1Id;
  }

  public String getType1Name() {
    return type1Name;
  }

  public void setType1Name(String type1Name) {
    this.type1Name = type1Name;
  }

  public int getType2Id() {
    return type2Id;
  }

  public void setType2Id(int type2Id) {
    this.type2Id = type2Id;
  }

  public String getType2Name() {
    return type2Name;
  }

  public void setType2Name(String type2Name) {
    this.type2Name = type2Name;
  }


}
