package beans;

import java.io.Serializable;

public class NatureBeans implements Serializable {
  private int natureId;
  private String natureName;



  public int getNatureId() {
    return natureId;
  }

  public void setNatureId(int natureId) {
    this.natureId = natureId;
  }

  public String getNatureName() {
    return natureName;
  }

  public void setNatureName(String natureName) {
    this.natureName = natureName;
  }

}
