package beans;

import java.io.Serializable;

public class PartyBeans implements Serializable {
  private int partyId;
  private String partyName;
  private boolean doShare;
  private int userId;




  public int getPartyId() {
    return partyId;
  }

  public void setPartyId(int partyId) {
    this.partyId = partyId;
  }

  public String getPartyName() {
    return partyName;
  }

  public void setPartyName(String partyName) {
    this.partyName = partyName;
  }

  public boolean isDoShare() {
    return doShare;
  }

  public void setDoShare(boolean doShare) {
    this.doShare = doShare;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

}
