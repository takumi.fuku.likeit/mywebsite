package beans;

import java.io.Serializable;

public class PartyDetailBeans implements Serializable {
  private int detailId;
  private int partyId;
  private int pokemonId;
  private String pokemonName;
  private int itemId;
  private String itemName;
  private int natureId;
  private String natureName;
  private String move1;
  private String move2;
  private String move3;
  private String move4;
  private String selectedAbility;





  public int getDetailId() {
    return detailId;
  }

  public void setDetailId(int detailId) {
    this.detailId = detailId;
  }

  public String getPokemonName() {
    return pokemonName;
  }

  public void setPokemonName(String pokemonName) {
    this.pokemonName = pokemonName;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getNatureName() {
    return natureName;
  }

  public void setNatureName(String natureName) {
    this.natureName = natureName;
  }

  public String getMove1() {
    return move1;
  }

  public void setMove1(String move1) {
    this.move1 = move1;
  }

  public String getMove2() {
    return move2;
  }

  public void setMove2(String move2) {
    this.move2 = move2;
  }

  public String getMove3() {
    return move3;
  }

  public void setMove3(String move3) {
    this.move3 = move3;
  }

  public String getMove4() {
    return move4;
  }

  public void setMove4(String move4) {
    this.move4 = move4;
  }

  public String getSelectedAbility() {
    return selectedAbility;
  }

  public void setSelectedAbility(String selectedAbility) {
    this.selectedAbility = selectedAbility;
  }

  public int getPartyId() {
    return partyId;
  }

  public void setPartyId(int partyId) {
    this.partyId = partyId;
  }

  public int getPokemonId() {
    return pokemonId;
  }

  public void setPokemonId(int pokemonId) {
    this.pokemonId = pokemonId;
  }

  public int getItemId() {
    return itemId;
  }

  public void setItemId(int itemId) {
    this.itemId = itemId;
  }

  public int getNatureId() {
    return natureId;
  }

  public void setNatureId(int natureId) {
    this.natureId = natureId;
  }

}
