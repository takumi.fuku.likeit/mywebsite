package beans;

import java.io.Serializable;

public class TypeBeans implements Serializable {
  private int typeId;
  private String typeName;


  public int getTypeId() {
    return typeId;
  }

  public void setTypeId(int typeId) {
    this.typeId = typeId;
  }

  public String getTypeName() {
    return typeName;
  }

  public void setTypeName(String typeName) {
    this.typeName = typeName;
  }

}
